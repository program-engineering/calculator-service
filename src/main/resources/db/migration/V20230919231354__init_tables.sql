CREATE TABLE calculation
(
    id         BIGSERIAL PRIMARY KEY,
    expression TEXT,
    result     TEXT,
    error      VARCHAR(255),
    code       INTEGER
);
