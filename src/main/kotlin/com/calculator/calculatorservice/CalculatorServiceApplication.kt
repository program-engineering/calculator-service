package com.calculator.calculatorservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CalculatorServiceApplication

fun main(args: Array<String>) {
    runApplication<CalculatorServiceApplication>(*args)
}
