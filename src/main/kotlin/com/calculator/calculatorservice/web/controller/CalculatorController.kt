package com.calculator.calculatorservice.web.controller

import com.calculator.calculatorservice.Calculator
import com.calculator.calculatorservice.exception.ControllerException
import com.calculator.calculatorservice.web.dto.CalculateRequest
import com.calculator.calculatorservice.web.dto.CalculateResponse
import com.calculator.calculatorservice.web.entity.Calculation
import com.calculator.calculatorservice.web.repository.CalculationRepository
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
class CalculatorController(var repository: CalculationRepository) {

    @PostMapping("/calculate", produces = ["application/json"])
    @ApiResponses(
        value =
        [
            ApiResponse(
                description = "Success",
                responseCode = "200",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = CalculateResponse::class),
                    ),
                ],
            ),
            ApiResponse(
                description = "Invalid request",
                responseCode = "400",
                content = [Content()],
            ),
            ApiResponse(
                description = "Internal server error",
                responseCode = "500",
                content = [Content()],
            ),
        ],
    )
    fun calculateExpression(@RequestBody request: CalculateRequest): CalculateResponse {
        try {
            val result = calculate(request.expression)
            repository.save(Calculation(request.expression, result.toPlainString(), null, 200))

            return CalculateResponse(result.toPlainString())
        } catch (e: Throwable) {
            repository.save(Calculation(request.expression, null, e.toString(), 400))
            throw ControllerException()
        }
    }

    @GetMapping("/get-list", produces = ["application/json"])
    @ApiResponses(
        value =
        [
            ApiResponse(
                description = "Success",
                responseCode = "200",
                content = [
                    Content(
                        mediaType = "application/json",
                        array = ArraySchema(
                            items = Schema(implementation = Calculation::class),
                            schema = Schema(implementation = Calculation::class),
                        ),
                    ),
                ],
            ),
            ApiResponse(
                description = "Internal server error",
                responseCode = "500",
                content = [Content()],
            ),
        ],
    )
    private fun getCalculationsList(): List<Calculation> = repository.findAll().toList()

    private fun calculate(expr: String?): BigDecimal {
        return Calculator.eval(expr)
    }
}
