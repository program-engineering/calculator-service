package com.calculator.calculatorservice.web.dto

data class CalculateRequest(val expression: String?)
