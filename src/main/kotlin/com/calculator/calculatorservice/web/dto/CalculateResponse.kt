package com.calculator.calculatorservice.web.dto

data class CalculateResponse(var result: String)
