package com.calculator.calculatorservice.web.repository

import com.calculator.calculatorservice.web.entity.Calculation
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CalculationRepository : CrudRepository<Calculation, Long>
