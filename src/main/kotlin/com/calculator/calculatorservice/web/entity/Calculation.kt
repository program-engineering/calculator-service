package com.calculator.calculatorservice.web.entity

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Table

@Table(name = "Calculation")
@Entity
class Calculation(
    val expression: String?,
    val result: String?,
    val error: String?,
    val code: Int,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,
)
