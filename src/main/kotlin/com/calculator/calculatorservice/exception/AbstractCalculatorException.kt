package com.calculator.calculatorservice.exception

import org.springframework.http.HttpStatusCode

abstract class AbstractCalculatorException(
    val status: HttpStatusCode,
    val payload: Map<String, Any> = mutableMapOf(),
    cause: Throwable? = null,
) : RuntimeException(cause)
