package com.calculator.calculatorservice.exception

import org.springframework.http.HttpStatus

open class ControllerException : AbstractCalculatorException(
    status = HttpStatus.BAD_REQUEST,
)
