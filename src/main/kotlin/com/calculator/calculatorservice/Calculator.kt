package com.calculator.calculatorservice

import com.github.keelar.exprk.Expressions
import java.math.BigDecimal

object Calculator {
    private val expr = Expressions()

    /*
     * @throws ExpressionException в случае некорректного синтаксического выражения.
     * @throws ArithmeticException в случае запрещенных арифметических операций.
     *  */
    fun eval(expression: String?): BigDecimal {
        // в случае null лучше кидать исключение или подавать пустую строку?
        return expr.eval(expression ?: "")
    }

    /*
     * Обрабатывает все исключения, бросаемые Expression().eval(expr).
     *
     * @return Результат вычисления, либо значние ошибки.
     *  */
    fun evalToString(expression: String?): String {
        return expr.evalToString(expression ?: "")
    }

    /*
     * По умолчанию precision:=18.
     *
     * @throws IllegalArgumentException – if the setPrecision parameter is less than zero.
     * @throws NullPointerException – if the rounding mode argument is null
     * */
    fun setPrecision(precision: Int) {
        expr.setPrecision(precision)
    }
}
