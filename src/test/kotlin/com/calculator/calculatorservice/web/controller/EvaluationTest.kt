package com.calculator.calculatorservice.web.controller

import com.calculator.calculatorservice.Calculator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.fail
import java.math.BigDecimal

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class EvalTest {
    @Nested
    inner class AdditionTests {
        @Test
        fun simpleAddition1() {
            assertEquals(BigDecimal(42), Calculator.eval("40 + 2"))
        }

        @Test
        fun simpleAddition2() {
            assertEquals(BigDecimal(0), Calculator.eval("0 + 0"))
        }

        @Test
        fun simpleAddition3() {
            assertEquals(BigDecimal(0), Calculator.eval("-40 + 40"))
        }

        @Test
        fun simpleAddition4() {
            assertEquals(BigDecimal(0), Calculator.eval("40 + -40"))
        }
    }

    @Nested
    inner class SubtractionTests {
        @Test
        fun simpleSubtraction1() {
            assertEquals(BigDecimal(500), Calculator.eval("900 - 400"))
        }

        @Test
        fun simpleSubtraction2() {
            assertEquals(BigDecimal(0), Calculator.eval("0 - 0"))
        }

        @Test
        fun simpleSubtraction3() {
            assertEquals(BigDecimal(0), Calculator.eval("1 - 1"))
        }

        @Test
        fun simpleSubtraction4() {
            assertEquals(BigDecimal(-400), Calculator.eval("0 - 400"))
        }
    }

    @Nested
    inner class MakeNegativeTests {
        @Test
        fun simpleMakeNegativeTest1() {
            assertEquals(BigDecimal(0), Calculator.eval("-0"))
        }
        @Test
        fun simpleMakeNegativeTest2() {
            assertEquals(BigDecimal(-100), Calculator.eval("-100"))
        }
        @Test
        fun simpleMakeNegativeTes3() {
            assertEquals(BigDecimal(0), Calculator.eval("--0"))
        }
        @Test
        fun simpleMakeNegativeTes4() {
            assertEquals(BigDecimal(4), Calculator.eval("--4"))
        }
    }

    @Nested
    inner class RemainderTests {
        @Test
        fun simpleRemainderTest1() {
            assertEquals(BigDecimal(0), Calculator.eval("5 % 5"))
        }
        @Test
        fun simpleRemainderTest2() {
            try {
                Calculator.eval("100 % 0")
                fail("Expected exception")
            } catch (_: Exception) {
            }
        }
        @Test
        fun simpleRemainderTest3() {
            try {
                Calculator.eval("0 % 0")
                fail("Expected exception")
            } catch (_: Exception) {
            }
        }
        @Test
        fun simpleRemainderTest4() {
            assertEquals(BigDecimal(0), Calculator.eval("-10 % 5"))
        }
        @Test
        fun simpleRemainderTest5() {
            assertEquals(BigDecimal(-1), Calculator.eval("-10 % 3"))
        }
        @Test
        fun simpleRemainderTest6() {
            assertEquals(BigDecimal(0), Calculator.eval("10 % -5"))
        }
        @Test
        fun simpleRemainderTest7() {
            assertEquals(BigDecimal(1), Calculator.eval("10 % -3"))
        }
        @Test
        fun simpleRemainderTest8() {
            assertEquals(BigDecimal(-1), Calculator.eval("-10 % -3"))
        }
        @Test
        fun simpleRemainderTest9() {
            assertEquals(BigDecimal("0.5123"), Calculator.eval("1.5123 % 1"))
        }
    }

    @Nested
    inner class MultiplicationTests {
        @Test
        fun simpleMultiplicationTest1() {
            assertEquals(BigDecimal(0), Calculator.eval("0 * 100"))
        }
        @Test
        fun simpleMultiplicationTest2() {
            assertEquals(BigDecimal(0), Calculator.eval("100 * 0"))
        }
        @Test
        fun simpleMultiplicationTes3() {
            val precision = 16
            Calculator.setPrecision(40)

            assertEquals(BigDecimal("1219326311370217952237463801111263526900"), Calculator.eval("12345678901234567890 * 98765432109876543210"))

            Calculator.setPrecision(precision)
        }
        @Test
        fun simpleMultiplicationTest4() {
            assertEquals(BigDecimal("9E-27"), Calculator.eval("0.9 * 0.00000000000000000000000001"))
        }
    }

    @Nested
    inner class DivisionTests {
        @Test
        fun simpleDivisionTest1() {
            try {
                Calculator.eval("0 / 0")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun simpleDivisionTest2() {
            try {
                Calculator.eval("-100 / 0")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun simpleDivisionTest3() {
            assertEquals(BigDecimal(1), Calculator.eval("1 / 1"))
        }
        @Test
        fun simpleDivisionTest4() {
            assertEquals(BigDecimal("1"), Calculator.eval("1 / 1"))
        }
        @Test
        fun simpleDivisionTest5() {
            assertEquals(BigDecimal("-20"), Calculator.eval("100 / -5"))
        }
        @Test
        fun simpleDivisionTest6() {
            assertEquals(BigDecimal("20"), Calculator.eval("-100 / -5"))
        }
    }

    @Nested
    inner class BracesTests {
        @Test
        fun bracesTestCorrect1() {
            assertEquals(BigDecimal(2), Calculator.eval("(1 + 1) / 1"))
        }
        @Test
        fun bracesTestCorrect2() {
            assertEquals(BigDecimal(0), Calculator.eval("(1 - 1) / 1"))
        }
        @Test
        fun bracesTestCorrect3() {
            assertEquals(BigDecimal(-2), Calculator.eval("(1 + 1) / -1"))
        }
        @Test
        fun bracesTestCorrect4() {
            assertEquals(BigDecimal(1), Calculator.eval("1 + (1 % 1)"))
        }
        @Test
        fun bracesTestCorrect5() {
            assertEquals(BigDecimal(2), Calculator.eval("((((((1)) + ((1)))))) / ((1))"))
        }
        @Test
        fun bracesTestCorrect6() {
            assertEquals(BigDecimal(0), Calculator.eval("(0) + (-0) - (0)"))
        }
        @Test
        fun bracesTestCorrect7() {
            assertEquals(BigDecimal(100000), Calculator.eval("((((100000))))"))
        }
        @Test
        fun bracesTestCorrect8() {
            assertEquals(BigDecimal(25), Calculator.eval("5 * (2 + 3)"))
        }
        @Test
        fun bracesTestCorrect9() {
            assertEquals(BigDecimal(4), Calculator.eval("(17 * 2) % (5 + 1)"))
        }

        @Test
        fun bracesTestFail1() {
            try {
                Calculator.eval("(1 + 2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail2() {
            try {
                Calculator.eval("1( + 2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail3() {
            try {
                Calculator.eval(")1 + 2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail4() {
            try {
                Calculator.eval("1) + 2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail5() {
            try {
                Calculator.eval("1 + (2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail6() {
            try {
                Calculator.eval("1 + )2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail7() {
            try {
                Calculator.eval("1 + 2(")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail8() {
            try {
                Calculator.eval("1 + 2)")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail9() {
            try {
                Calculator.eval("((1 + 2)")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail10() {
            try {
                Calculator.eval("(1)) + 2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail11() {
            try {
                Calculator.eval("(1 + 2)))")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail12() {
            try {
                Calculator.eval("()()()()1 + 2")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail13() {
            try {
                Calculator.eval("(1 + 2) + 1)")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun bracesTestFail14() {
            try {
                Calculator.eval("((1 + 2) + )3")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }

        @Test
        fun bracesTestFail15() {
            try {
                Calculator.eval("((1 + 2) + )3)")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }

        @Test
        fun bracesTestFail16() {
            try {
                Calculator.eval("((1 + 2) + 3))")
                fail("exception expected")
            } catch (_: Exception) {
            }
        }
    }

    @Nested
    inner class ExpressionTests {
        @Test
        fun expressionTestSuc1() {
            assertEquals(BigDecimal(2), Calculator.eval("1 + 2 * 3 % 5"))
        }
        @Test
        fun expressionTestSuc2() {
            assertEquals(BigDecimal(9), Calculator.eval("(1 + 2) * (3 % 5)"))
        }
        @Test
        fun expressionTestSuc3() {
            assertEquals(BigDecimal(1), Calculator.eval("(1 + 2) / 3"))
        }
        @Test
        fun expressionTestSuc4() {
            assertEquals(BigDecimal(0), Calculator.eval("5 % 5 * 3"))
        }
        @Test
        fun expressionTestSuc5() {
            assertEquals(BigDecimal(12), Calculator.eval("14 % 5 * 3"))
        }
        @Test
        fun expressionTestSuc6() {
            assertEquals(BigDecimal("0.0"), Calculator.eval("(1 + 2) * (5 / 2) * 2 % 5"))
        }
        @Test
        fun expressionTestSuc7() {
            assertEquals(BigDecimal(28), Calculator.eval("((1 + 2) + 3) + (4 + (5 + 6) + 7)"))
        }
        @Test
        fun expressionTestSuc8() {
            assertEquals(BigDecimal(-23 * 4), Calculator.eval("(23) + (-23) * 5"))
        }
        @Test
        fun expressionTestSuc9() {
            assertEquals(BigDecimal((((1))) + (2) * 5), Calculator.eval("(((1))) + (2) * 5"))
        }
        @Test
        fun expressionTestFailure1() {
            try {
                Calculator.eval("1 + 2 * 3 % % 5")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure2() {
            try {
                Calculator.eval("1 + + 2 * 3 % 5")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure3() {
            try {
                Calculator.eval("1 + 2 * + 3 % % 5")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure4() {
            try {
                Calculator.eval("1 + 1 2 * 3 % % 5")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure5() {
            try {
                Calculator.eval("1 1")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure6() {
            try {
                Calculator.eval("1 1 + 1")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure7() {
            try {
                Calculator.eval("17 + ()")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure8() {
            try {
                Calculator.eval("+ 1 + 2")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure9() {
            try {
                Calculator.eval("/ 1 + 2")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure10() {
            try {
                Calculator.eval("/")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionFailure11() {
            try {
                Calculator.eval("5 % 5 /")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure12() {
            try {
                Calculator.eval("5 % 5 -")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure13() {
            try {
                Calculator.eval("5 % 5 +")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure14() {
            try {
                Calculator.eval("5 % 5 *")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
        @Test
        fun expressionTestFailure15() {
            try {
                Calculator.eval("() - ()")
                fail("Exception expected")
            } catch (_: Exception) {
            }
        }
    }
}
