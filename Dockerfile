FROM openjdk:17-slim as jar-builder

RUN apt-get update && apt-get install -y \
    curl \
    unzip \
    zip \
    ca-certificates \
    gnupg \
    default-jdk

RUN install -m 0755 -d /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    chmod a+r /etc/apt/keyrings/docker.gpg && \
    echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt-get update && apt-get install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io

RUN curl -s https://get.sdkman.io | bash

RUN bash -c 'source /root/.sdkman/bin/sdkman-init.sh && sdk install kotlin'

COPY . /app
RUN cd /app && \
    ./gradlew build -x test

WORKDIR /app
ENTRYPOINT ["./gradlew", "test"]


FROM openjdk:17-slim as app

COPY --from=jar-builder /app/build/libs/calculator-service-0.0.1-SNAPSHOT.jar /app/app.jar

EXPOSE 8000
CMD ["java", "-jar", "/app/app.jar"]
