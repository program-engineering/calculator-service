# Инструкции по работе с виртуальной машиной

## Общие инструкции (нужны тольк при первичной настройке, оставляю тут чтобы не потерять в будущем)

1. Установить zip, uzip, sdkman:

```bash
sudo apt install zip, uzip
curl -s "https://get.sdkman.io" | bash
```

2. Установть kotlin, gradle:

```bash
sdk install kotlin
sdk install gradle
```

3. Установить docker (https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository):

## Инструкции для разработчиков: